FROM golang:alpine AS builder
WORKDIR /go/src/app
COPY . .

RUN CGO_ENABLED=0 go build -o app .

FROM alpine:latest
RUN apk add --no-cache ca-certificates
COPY --from=builder /go/src/app/app /app

CMD ["./app"]
