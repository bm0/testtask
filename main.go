package main

import (
	"net/http"
	"net"
	"time"
	"flag"
	"log"
	"bufio"
	"os"
)

func init() {
	log.SetFlags(0)
}

func main() {
	quota := flag.Int("workers", 5, "allows to set the number of simultaneous workers")
	flag.Parse()

	if *quota < 1 {
		log.Fatalf("count of workers must be greater than zero")
	}

	scanner := bufio.NewScanner(os.Stdin)
	client := &http.Client{
		Transport: &http.Transport{
			DialContext: (&net.Dialer{
				Timeout:   60 * time.Second,
				KeepAlive: 60 * time.Second,
			}).DialContext,
			TLSHandshakeTimeout:   20 * time.Second,
			ResponseHeaderTimeout: 20 * time.Second,
			ExpectContinueTimeout: 5 * time.Second,
		},
		Timeout: time.Second * 60,
	}

	wp := NewWorkerPool(*quota, client)

	for scanner.Scan() {
		if err := scanner.Err(); err != nil {
			log.Printf("Input error: %s", err)
			continue
		}

		if err := wp.Task(scanner.Text());err != nil {
			log.Println(err)
		}
	}

	wp.Wait()
	log.Printf("Total: %d", wp.GetTotal())
}
