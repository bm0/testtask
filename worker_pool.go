package main

import (
	"sync"
	"net/http"
	"net/url"
	"log"
	"sync/atomic"
	"io/ioutil"
	"bytes"
)

// A WorkerPool is a instrument to control workers.
type WorkerPool struct {
	wg      *sync.WaitGroup
	client  *http.Client

	quotaCh chan struct{}
	counter uint32
}

// NewWorkerPool returns new WorkerPool.
func NewWorkerPool(quota int, client *http.Client) *WorkerPool {
	if client == nil {
		client = http.DefaultClient
	}

	return &WorkerPool{
		new(sync.WaitGroup),
		client,
		make(chan struct{}, quota),
		0,
	}
}

// Task adds a task to the queue for execution.
// If the quota is exceeded, the execution thread will be blocked
// until one of the workers is released.
func (r *WorkerPool) Task(address string) error {
	if _, err := url.Parse(address); err != nil {
		return err
	}

	r.quotaCh <- struct{}{}
	r.wg.Add(1)

	go r.worker(address)
	return nil
}

// GetTotal returns current counter value, it Thread-safe operation.
func (r *WorkerPool) GetTotal() uint32 {
	return atomic.LoadUint32(&r.counter)
}

// Wait blocks until the WaitGroup counter is zero.
func (r *WorkerPool) Wait() {
	r.wg.Wait()
}

// Reset waiting for all workers then resets the counter.
func (r *WorkerPool) Reset()  {
	r.wg.Wait()
	atomic.StoreUint32(&r.counter, 0)
}

// Worker contains the logic of crawling addresses and counting
// occurrences of the substring "Go".
func (r *WorkerPool) worker(address string) {
	defer func() {
		r.wg.Done()
		<-r.quotaCh
	}()

	resp, err := r.client.Get(address)
	if err != nil {
		log.Println(err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}

	count := bytes.Count(body, []byte("Go"))
	atomic.AddUint32(&r.counter, uint32(count))

	log.Printf("Count for %s: %d", address, count)
}
