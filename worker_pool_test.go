package main

import (
	"testing"
	"net/http/httptest"
	"net/http"
	"strings"
	"io"
)

func TestWorkerPool(t *testing.T) {
	var cases = []struct{
		desc string
		total uint32
	}{
		{"zero entries", 0},
		{"five entries", 5},
		{"million entries", 1000000},
	}

	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				io.WriteString(w, strings.Repeat("Go ", int(tc.total)))
			}))

			wp := NewWorkerPool(5, nil)
			wp.Task(ts.URL)
			wp.Wait()

			ts.Close()

			if wp.GetTotal() != tc.total {
				t.Fatalf("values are not identical")
			}
		})
	}
}
